import static io.restassured.RestAssured.basePath;
import static io.restassured.RestAssured.baseURI;

public abstract class BaseTest {

    public static void preCondition(){
        baseURI = "https://jsonplaceholder.typicode.com";
        basePath = "todos/";
    }
}

