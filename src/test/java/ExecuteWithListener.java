
import org.junit.runner.JUnitCore;

public class ExecuteWithListener {
    public static void main(String[] args)
    {
        JUnitCore runner = new JUnitCore();
        runner.addListener(new ExecutionListener());
        runner.run(TestScenarios.class);
    }
}
