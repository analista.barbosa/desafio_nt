import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import io.restassured.response.ValidatableResponse;
import org.apache.http.HttpStatus;
import org.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;

import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchemaInClasspath;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@RunWith(DataProviderRunner.class)
public class TestScenarios {

    @Test
    public void validateContract() {
        ValidatableResponse response =
                given()
                .when()
                        .get("https://jsonplaceholder.typicode.com/todos/1")
                .then().statusCode(HttpStatus.SC_OK);
        response.body(matchesJsonSchemaInClasspath("contract.json"));
    }

    @Test
    public void validateGet() {
        ValidatableResponse response =
                given()
                        .queryParam("type", "json")
                .when()
                        .get("https://jsonplaceholder.typicode.com/todos/1")
                .then().statusCode(HttpStatus.SC_OK);
        response.body("id", equalTo(1));
        response.body("userId", equalTo(1));
        response.body("title", notNullValue());
        response.body("completed", equalTo(false));
    }

    @Test
    public void validateDelete() {
        ValidatableResponse response =
                given()
                        .queryParam("type", "json")
                        .when()
                        .delete("https://jsonplaceholder.typicode.com/todos/1")
                        .then().statusCode(HttpStatus.SC_NO_CONTENT);
    }

    @Test
    public void validatePut() {
        JSONObject requestParams = new JSONObject();
        requestParams.put("title", "This is the new title");
        requestParams.put("body", "bablablablabll");


        ValidatableResponse response =
                given()
                        .queryParam("type", "json")
                        .body(requestParams.toString())
                .when()
                        .put("https://jsonplaceholder.typicode.com/todos/1")
                .then()
                        .statusCode(HttpStatus.SC_OK);
    }

    @Test
    @UseDataProvider("dataGenerator")
    public void validatePost(String title, String message) {
        JSONObject requestParams = new JSONObject();
        requestParams.put("title", title);
        requestParams.put("body", message);

        ValidatableResponse response =
                given()
                        .queryParam("type", "json")
                        .body(requestParams.toString())
                .when()
                        .post("https://jsonplaceholder.typicode.com/todos")
                .then()
                        .statusCode(HttpStatus.SC_CREATED);
    }

    @Test
    public void validate404Error() {
        ValidatableResponse response =
                given()
                        .queryParam("type", "json")
                .when()
                        .get("https://jsonplaceholder.typicode.com/todoXXs/1")
                .then().statusCode(HttpStatus.SC_NOT_FOUND);
    }

    @Test
    public void validateInvalidPayload() {
        JSONObject requestParams = new JSONObject();

        ValidatableResponse response =
                given()
                        .queryParam("type", "json")
                        .body(requestParams.toString())
                .when()
                        .post("https://jsonplaceholder.typicode.com/todos")
                .then()
                        .statusCode(HttpStatus.SC_BAD_REQUEST);
    }

    @DataProvider
    public static Object[][] dataGenerator()
    {
        return new Object[][]
                {
                        {"testCaseOne","Felipe Barbosa"},
                        {"testCaseTwo","NTConsult"}
                };
    }
}
